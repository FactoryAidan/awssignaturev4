const ASV4_Helper	=	require('./Helper.js');

module.exports	=	class AwsSignatureV4_Signature{

	constructor(Params){
		this.Params	=	Params;
	}

	toString(){
		return this.generateSignature();
	}

	setStringToSign(StringToSign){
		this.StringToSign	=	StringToSign;
		return this;
	}

	//	Verified Functional
	//	@return	signing_key	{Buffer}
	private_generateSigningKey(){

		const key_seed		=	`AWS4${this.Params.aws.secretAccessKey}`;
		const key_date		=	ASV4_Helper.sha256Hmac(ASV4_Helper.getDate_YYYYMMDD(this.Params),key_seed);
		const key_region	=	ASV4_Helper.sha256Hmac(this.Params.aws.region,key_date);
		const key_service	=	ASV4_Helper.sha256Hmac(this.Params.aws.service,key_region);
		const signing_key	=	ASV4_Helper.sha256Hmac('aws4_request',key_service);

		return signing_key;
	}

	//	Verified Functional
	generateSignature(){

		const signature	=	ASV4_Helper.sha256Hmac(
			this.StringToSign.toString(),
			this.private_generateSigningKey(),
			'hex'
		);

		return signature;

	}

}//class