module.exports	=	class AwsSignatureV4_Parameters{

	required_aws_properties	=	['accessKeyId','region','secretAccessKey','service'];

	constructor(){

		this.aws		=	{};
		this.date		=	new Date;
		this.http		=	{};
		this.location	=	{};

	}

//	------------
//	Initializers
//	------------
	static fromFetch(given_fetch_url,given_fetch_options){
		const Location	=	new URL(given_fetch_url);
		const request_params	=	{
			http	:	{
				method		:	given_fetch_options.method,
				headers		:	given_fetch_options.headers,
				body		:	given_fetch_options.body,
			},
			location:	{
				host		:	Location.host,
				pathname	:	Location.pathname,
				search		:	Location.search,
			},
		}
		const Instance	=	new AwsSignatureV4_Parameters
		Instance.setRequestParameters(request_params);
		return Instance;
	}

	setRequestParameters(given_request_params){
		this.http	=	{
			method		:	this.getHttpMethod(given_request_params.http),
			headers		:	this.getHttpHeaders(given_request_params.http),
			body		:	this.getHttpBody(given_request_params.http),
		};
		this.location	=	{
			host		:	this.getLocationHost(given_request_params.location),
			pathname	:	this.getLocationPathname(given_request_params.location),
			search		:	this.getLocationSearch(given_request_params.location),
		};
		return this;
	}

	setAwsIamCredentials(AwsIamCredentials){
		this.aws	=	{
			region			:	this.getAwsRegion(AwsIamCredentials),
			service			:	this.getAwsService(AwsIamCredentials),
			accessKeyId		:	this.getAwsAccessKeyId(AwsIamCredentials),
			secretAccessKey	:	this.getAwsSecretAccessKey(AwsIamCredentials),
			sessionToken	:	this.getAwsSessionToken(AwsIamCredentials),
		};
		console.log('AWSIamCredentials:');
		console.log(this.aws);
		return this;
	}

//	---------------
//	Getters/Setters
//	---------------
	get aws(){
		//	Validate the object is set will all required properties.
		const validation_errors	=	new Array;
		this.required_aws_properties.forEach(key=>{
			if( !this._aws[key] )	validation_errors.push(`🔥 Remember to set .${key} in the AwsIamCredentials object you pass. Like: .withAwsIamCredentials({${key}:'some_value'})`);
		});
		if( validation_errors.length )	throw validation_errors.join("\n");
		return this._aws;
	}
	set aws(object){
		this._aws	=	object;
	}

//	----
//	HTTP
//	----
	getHttpMethod(given_http={}){
		if( !given_http.method )	return 'GET';
		return given_http.method.toUpperCase().trim();
	}
	getHttpHeaders(given_http={}){
		if( !given_http.headers )	return {};

		//	Sanitize Headers | Meaning lowercase all of them so there are no case-sensitive duplicates
		Object.keys(given_http.headers).forEach(function(key,i,a){
			const new_key	=	key.toLowerCase();
			if( key != new_key ){
				let value	=	given_http.headers[key];	//	Current Value
				given_http.headers[new_key]	=	value;		//	Set into new lowercased key
				delete given_http.headers[key]				//	Remove old uppercased key
			}
		});
	
		return given_http.headers;
	}
	getHttpBody(given_http={}){
		return given_http.body || '';
	}

//	--------
//	LOCATION
//	--------
	getLocationHost(given_location={}){
		return given_location.host || '';
	}
	getLocationPathname(given_location={}){
		return given_location.pathname || '';
	}
	getLocationSearch(given_location={}){
		return given_location.search || '';
	}

//	---
//	AWS
//	---
	getAwsRegion(given_aws={}){
		const string	=	given_aws.region || process.env.AWS_REGION;
		if( !string )	throw ("🔥 Remember to set either process.env.AWS_REGION or .withAwsIamCredentials({region:'us-east-1|us-west-2'})");
		return string;
	}
	getAwsService(given_aws={}){
		const string	=	given_aws.service || process.env.AWS_SERVICE;
		if( !string )	throw ("🔥 Remember to set either process.env.AWS_SERVICE or .withAwsIamCredentials({service:'iotdata|s3'})");
		return string;
	}
	getAwsAccessKeyId(given_aws={}){
		const string	=	given_aws.accessKeyId || process.env.AWS_ACCESS_KEY_ID;
		if( !string )	throw ("🔥 Remember to set either process.env.AWS_ACCESS_KEY_ID or .withAwsIamCredentials({accessKeyId:'XXXXXXXXXXXXXXXXXXXX'})");
		return string;
	}
	getAwsSecretAccessKey(given_aws={}){
		const string	=	given_aws.secretAccessKey || process.env.AWS_SECRET_ACCESS_KEY;
		if( !string )	throw ("🔥 Remember to set either process.env.AWS_SECRET_ACCESS_KEY or .withAwsIamCredentials({secretAccessKey:'XX/XXXXXXXXXXXXXXXXXXXXXXXXXXXX+XXXXXXXX'})");
		return string;
	}
	getAwsSessionToken(given_aws={}){
		const string	=	given_aws.sessionToken || process.env.AWS_SESSION_TOKEN;
		if( !string )	throw ("🔥 Remember to set either process.env.AWS_SESSION_TOKEN or .withAwsIamCredentials({sessionToken:'a_very_long_token_string'})");
		return string;		
	}


}//class

module.exports.prototype.setTestData	=	function(desired_test_index=0){
	switch(desired_test_index){
		case 0:
			this.http		=	{
				method	:	'GET',
				headers	:	{
					Host					:	'examplebucket.s3.amazonaws.com',
					'x-amz-date'			:	'20130524T000000Z',
					Range					:	'bytes=0-9',
					'x-amz-content-sha256'	:	'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855',
				},
				body	:	'',
			};
			this.location	=	{
				pathname	:	'/test.txt',
				search		:	'',
			};
			this.aws	=	{
				accessKeyId		:	'AKIAIOSFODNN7EXAMPLE',
				secretAccessKey	:	'wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY',
				region			:	'us-east-1',
				service			:	's3',
			};
			this.date	=	new Date('Fri, 24 May 2013 00:00:00 GMT');
			break;
		case 1:
			this.http		=	{
				method	:	'PUT',
				headers	:	{
					Host					:	'examplebucket.s3.amazonaws.com',
					Date					:	'Fri, 24 May 2013 00:00:00 GMT',
					'x-amz-date'			:	'20130524T000000Z',
					'x-amz-storage-class'	:	'REDUCED_REDUNDANCY',
					'x-amz-content-sha256'	:	'44ce7dd67c959e0d3524ffac1771dfbba87d2b6b4b4e99e42034a8b803f8b072',
				},
				body	:	'Welcome to Amazon S3.',
			};
			this.location	=	{
				pathname	:	'/test$file.text',
				search		:	'',
			};
			this.aws	=	{
				accessKeyId		:	'AKIAIOSFODNN7EXAMPLE',
				secretAccessKey	:	'wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY',
				region			:	'us-east-1',
				service			:	's3',
			};
			this.date	=	new Date('Fri, 24 May 2013 00:00:00 GMT');
			break;
		case 2:
			this.http		=	{
				method	:	'GET',
				headers	:	{
					Host					:	'examplebucket.s3.amazonaws.com',
					'x-amz-date'			:	'20130524T000000Z',
					'x-amz-content-sha256'	:	'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855',
				},
				body	:	'',
			};
			this.location	=	{
				pathname	:	'',
				search		:	'?lifecycle',
			};
			this.aws	=	{
				accessKeyId		:	'AKIAIOSFODNN7EXAMPLE',
				secretAccessKey	:	'wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY',
				region			:	'us-east-1',
				service			:	's3',
			};
			this.date	=	new Date('Fri, 24 May 2013 00:00:00 GMT');
			break;
		case 3:
			this.http		=	{
				method	:	'GET',
				headers	:	{
					Host					:	'examplebucket.s3.amazonaws.com',
					'x-amz-date'			:	'20130524T000000Z',
					'x-amz-content-sha256'	:	'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855',
				},
				body	:	'',
			};
			this.location	=	{
				pathname	:	'',
				search		:	'?max-keys=2&prefix=J',
			};
			this.aws	=	{
				accessKeyId		:	'AKIAIOSFODNN7EXAMPLE',
				secretAccessKey	:	'wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY',
				region			:	'us-east-1',
				service			:	's3',
			};
			this.date	=	new Date('Fri, 24 May 2013 00:00:00 GMT');
			break;
	}//switch
	return this;
}//function
