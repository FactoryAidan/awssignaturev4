const ASV4_Helper	=	require('./Helper.js');

module.exports	=	class AwsSignatureV4_CanonicalRequest{

	constructor(ParametersClass){
		this.Params	=	ParametersClass;

		//	Some extra headers need to be added but those headers are based on
		//	Values generated within this class instead of the Paramters class.
		//	TO-DO:	Possibly move this functionality to Params class.
		this.addAdditionalAwsHeadersToParams();
	}

	toString(){
		return this.generateCanonicalRequest();
	}

	addAdditionalAwsHeadersToParams(){
		//	Note:
		//		The 'x-amz-content-sha256' header is required for all AWS Signature Version 4 requests.
		//		It provides a hash of the request payload (aka request.body ).
		//		If there is no payload, you must provide the hash of an empty string.
		this.Params.http.headers['x-amz-content-sha256']	=	this.generatePayloadHashAsHex();
		this.Params.http.headers['x-amz-date']				=	ASV4_Helper.getDate_YYYYMMDDTHHMMSSZ(this.Params);
		//	If there is already a http.headers.host written on the request, leave it in place.
		//	If it is capitalized, our own sanitizer will lowercase it for us.
		//	If there is not already a http.header.host written on the request, add it.
		if( !this.Params.http.headers.Host && !this.Params.http.headers.host )
			this.Params.http.headers['host']					=	this.Params.location.host;
			
	//	this.Params.http.headers['x-amz-security-token']	=	this.Params.aws.sessionToken;
	//	this.Params.http.headers['X-Amz-Security-Token']	=	this.Params.aws.sessionToken;
	}

	generateCanonicalRequest(){

		return [
			this.Params.http.method,
			this.generateCanonicalUri(),
			this.generateCanonicalQueryString(),
			this.generateCanonicalHeaders(),
			this.generateSignedHeaders(),
			this.generatePayloadHashAsHex(),
		].join("\n");
	}

	generateCanonicalUri(given_uri=this.Params.location.pathname){
		return encodeURI(given_uri);
	//	DEPRECATED:
	//	While the following works, there's no need to manually alter the default behavior of the above.
	//	The following was formed due to the documentation stating:
	//		- You don't encode the '/' in the path but you do retain it.
	//	return '/'+encodeURI(given_uri.replace(/^\//,''));
	}

	generateCanonicalQueryString(given_query_string=this.Params.location.search){
		if( !given_query_string )	return '';
		const parameters	=	given_query_string.replace(/^[\?]/,'').split('&');
		return parameters.map(function(p,i,a){
				const key_value	=	p.split('=');
				const key		=	key_value[0] ? encodeURIComponent(key_value[0].trim()) : '' ;
				const value		=	key_value[1] ? encodeURIComponent(key_value[1].trim()) : '' ;
				return `${key}=${value}`;
			})
			.sort()
			.join('&')
			;
	}

	generateCanonicalHeaders(given_headers_object=this.Params.http.headers){

		return this.getHeaderKeys(given_headers_object)
			.map(function(key,i,a){
				const value	=	given_headers_object[key];
				return `${key.toLowerCase().trim()}:${value.trim()}`;
			})
			.sort()
			.join("\n")
			+ "\n"	//	Defined to end with an extra trailing NewLine
			;
	}

	generateSignedHeaders(given_headers_object=this.Params.http.headers){
			return this.getHeaderKeys(given_headers_object)
			.map(key=>key.toLowerCase().trim())
			.sort()
			.join(';')
			;
	}
	generatePayloadHashAsHex(given_body_string=this.Params.http.body){
		return ASV4_Helper.sha256Hash(given_body_string,'hex');
	}
	
	
	//	------------------
	//	Helper Function(s)
	//	------------------
	getHeaderKeys(given_headers_object=this.Params.http.headers){
		return Object
			.keys(given_headers_object)
			.filter(key=>(!/^\s*authorization\s*$/i.test(key)))
			;
	}
	

}//class