const crypto	=	require('crypto');

module.exports	=	class AwsSignatureV4_Helper{

	static sha256Hash(given_string,desired_encoding=null){
		return crypto
			.createHash('sha256')
			.update(given_string)
			.digest(desired_encoding)
			;
	}

	static sha256Hmac(given_string,secret_key,desired_encoding=null){
		const algorithm	=	'sha256';
		return crypto
			.createHmac(algorithm,secret_key)
			.update(given_string)
			.digest(desired_encoding)
			;
	}

	static getDate_YYYYMMDD(Params){
		return Params.date.toISOString().split('T').shift().replace(/-/g,'');
	}

	//	AWS' version of "current UTC time in ISO 8601". ("20130524T000000Z")
	static getDate_YYYYMMDDTHHMMSSZ(Params){
		return Params.date.toISOString().replace(/[\-\.:Z]/ig,'').slice(0,-3)+'Z';
	}

}