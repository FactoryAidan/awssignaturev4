## AWS IoT can be authenticated in two ways:

- PKI Certificates
- AWS Signature V4

## AWS Signature V4

*This authenticate mechanism is used to authenticate more than just IoT.
It can be used to authenticate AWS S3 Bucket requests with IAM credentials.*

For this, we're going to be focused on consuming the following API:

- [IoT Device Shadow REST API](https://docs.aws.amazon.com/iot/latest/developerguide/device-shadow-rest-api.html)

### Getting Temporary IAM Credentials

[Table of Accepted Authorization Mechanisms](https://docs.aws.amazon.com/iot/latest/developerguide/iot-authorization.html)
[What to invoke to get Temporary IAM Credentials from a Cognito UserPool `IdToken`](https://docs.aws.amazon.com/cognito/latest/developerguide/getting-credentials.html#getting-credentials-1.javascript)
[More support to: What to invoke to get Temporary IAM Credentials from a Cognito UserPool `IdToken`](https://stackoverflow.com/a/43958394)

### Signing IAM Credentials into a HTTP request header

[AWS's VERY elaborate walkthrough of how](https://docs.aws.amazon.com/AmazonS3/latest/API/sig-v4-header-based-auth.html)

### Which header fields to include in the HTTP Request's SignedHeaders & Signature

See the note mentioned [here](https://docs.aws.amazon.com/general/latest/gr/sigv4-add-signature-to-request.html):
> When you add the X-Amz-Security-Token parameter to the query string, some services require that you include this parameter in the canonical (signed) request. For other services, you add this parameter at the end, after you calculate the signature. For details, see the API reference documentation for that service.

For IoT, the `X-Amz-Security-Token` parameter does NOT need to be signed into the request. It will, however, work either way whether you sign in into the request as a SignedHeader or add it as a stand-alone request header after calculation the Authorization header.


# Competitors

[NPM aws4](https://www.npmjs.com/package/aws4)
[NPM aws-signature-v4](https://github.com/department-stockholm/aws-signature-v4)
[NPM aws-signature-v4 index.js](https://github.com/department-stockholm/aws-signature-v4/blob/8766478d073a334e55d156336c6ecfe2d66d99df/index.js)
