const ASV4_Helper	=	require('./Helper.js');

module.exports	=	class AwsSignatureV4_StringToSign{

	constructor(Params){
		this.Params	=	Params;
	}

	setCanonicalRequest(CanonicalRequest){
		this.CanonicalRequest	=	CanonicalRequest;
		return this;
	}

	toString(CanonicalRequest){

		const string_to_sign	=	[
			'AWS4-HMAC-SHA256',
			ASV4_Helper.getDate_YYYYMMDDTHHMMSSZ(this.Params),
			this.generateScopeString(),
			ASV4_Helper.sha256Hash(this.CanonicalRequest.toString(),'hex'),
		].join("\n");

		return string_to_sign;

	}

	//	Verified Functional
	generateScopeString(){
		return [
			ASV4_Helper.getDate_YYYYMMDD(this.Params),	//	'20130524'
			this.Params.aws.region,						//	'us-east-1'
			this.Params.aws.service,					//	's3|iot'
			'aws4_request',
		].join('/');
	}

}//class