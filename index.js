const ASV4_Helper			=	require('./Helper.js');
const ASV4_Parameters		=	require('./Parameters.js');
const ASV4_CanonicalRequest	=	require('./CanonicalRequest.js');
const ASV4_StringToSign		=	require('./StringToSign.js');
const ASV4_Signature		=	require('./Signature.js');

module.exports	=	class AwsSignatureV4 {

	//	https://docs.aws.amazon.com/AmazonS3/latest/API/sig-v4-header-based-auth.html

	debugging	=	false;

	constructor(ParameterClass){
		this.Params	=	ParameterClass;
	}

	withTestData(test_data_index=0){
		this.Params.setTestData(test_data_index);
		return this;
	}
	withAwsIamCredentials(AwsIamCredentials){
// 		AwsIamCredentials	=	{
// 			region			:	AwsIamCredentials.region,		//	us-east-1|us-west-2
// 			service			:	AwsIamCredentials.service,		//	iot|s3
// 			accessKeyId		:	AwsIamCredentials.accessKeyId,
// 			secretAccessKey	:	AwsIamCredentials.secretAccessKey,
// 		};
		this.Params.setAwsIamCredentials(AwsIamCredentials);
		return this;
	}

	initialize(){
		this.CanonicalRequest	=	new ASV4_CanonicalRequest(this.Params);
		this.StringToSign		=	new ASV4_StringToSign(this.Params).setCanonicalRequest(this.CanonicalRequest);
		this.Signature			=	new ASV4_Signature(this.Params).setStringToSign(this.StringToSign);		

		//	-------
		//	Testing
		//	-------
		if( this.debugging ){
			const colors	=	{
				reset	:	"\x1b[0m",
				bg	:	{
					black	:	"\x1b[40m",
					red		:	"\x1b[41m",
					green	:	"\x1b[42m",
					yellow	:	"\x1b[43m",
					blue	:	"\x1b[44m",
					magenta	:	"\x1b[45m",
					cyan	:	"\x1b[46m",
					white	:	"\x1b[47m",
				},
			};
			
			console.group('Debugging Output:');
				console.log( colors.bg.blue+ '👷'+'CanonicalRequest'+'👇' );
				console.log( this.CanonicalRequest.toString() );
				console.log( colors.bg.magenta+ '👷'+'StringToSign'+'👇' );
				console.log( this.StringToSign.toString() );
				console.log( colors.bg.yellow+ '👷'+'Signature'+'👇');
				console.log( this.Signature.toString() );
				console.log( colors.reset);
			console.groupEnd('Debugging Output:');
		}

		return this;

	}

	generateAuthorizationHeaderValue(){

		this.initialize();

		const credential_string		=	`${this.Params.aws.accessKeyId}/${this.StringToSign.generateScopeString()}`;
		const signed_headers_string	=	this.CanonicalRequest.generateSignedHeaders();
		const signature_string		=	this.Signature.toString();

		return 'AWS4-HMAC-SHA256' + ' ' + [
			`Credential=${credential_string}`,
			`SignedHeaders=${signed_headers_string}`,
			`Signature=${signature_string}`,
		].join();
	}

	static fromFetch(given_fetch_url,given_fetch_options,given_aws_iam_credentials){
		if( !given_fetch_options )	throw "🔥 You must provide a 'node-fetch' literal options object. We will write to that same object."

		const Params	=	ASV4_Parameters.fromFetch(given_fetch_url,given_fetch_options);
		if(given_aws_iam_credentials)	Params.setAwsIamCredentials(given_aws_iam_credentials);

		const Instance	=	new AwsSignatureV4(Params);
		Instance.given_fetch_options	=	given_fetch_options;

		return Instance;
	}
	attachAuthorizationHeaderToFetchOptions(){
		const authorization_header_value	=	this.generateAuthorizationHeaderValue();
		if( !this.given_fetch_options.headers )	this.given_fetch_options.headers	=	{};
		this.given_fetch_options.headers.Authorization	=	authorization_header_value;
		return this;
	}

}//class